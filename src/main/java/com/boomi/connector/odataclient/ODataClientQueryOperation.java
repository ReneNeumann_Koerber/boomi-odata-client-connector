package com.boomi.connector.odataclient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.olingo.odata2.core.commons.Encoder;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.Expression;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.IOUtil;
import com.fasterxml.jackson.core.JsonParseException;

public class ODataClientQueryOperation extends BaseQueryOperation {

    public enum OperationProperties {MAXDOCUMENTS, PAGESIZE}
	QueryFilter _queryFilter=null;
   	long _maxDocuments;
   	long _pageSize;
   	String skipTokenPath=null;
   	OperationCookie _operationCookie;
	
	protected ODataClientQueryOperation(ODataClientConnection conn) {
		super(conn);
	}
	
	@Override
	protected void executeQuery(QueryRequest request, OperationResponse response) {
       	Logger logger = response.getLogger();
		getConnection().setLogger(logger);
        PropertyMap opProps = getContext().getOperationProperties();
       	_maxDocuments = getMaxDocuments(opProps);
       	_pageSize = getPageSize(opProps);
		if (getPageSizeQueryParam()!=null && _pageSize<=0)
			throw new ConnectorException("Must specify the Page Size to be greater than 0");
		String cookieString=getContext().getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT);		
		_operationCookie = new OperationCookie(cookieString);
		String pageItemPath = "/d/results/*"; //OData v2
		
        FilterData input = request.getFilter();
        if (input!=null)
        	_queryFilter = input.getFilter();
		String path = getContext().getObjectTypeId();
		CloseableHttpResponse httpResponse = null;
		//TODO each pathParam must have a filter term in the top level AND....if not throw an error
		try {
	        String uriParams="";
	        if (opProps.getBooleanProperty("ALLOWSELECT", false))
	        	uriParams = appendPath(uriParams, getSelectTermsQueryParam(this.getContext().getSelectedFields()));
	        uriParams = appendPath(uriParams, getExpandTermsQueryParameter(this.getContext().getSelectedFields()));
	    	if (input!=null && input.getFilter()!=null)
	    	{
	    		if (_queryFilter!=null)
	    		{
		    		String filter=getFilterTermsQueryParam(_queryFilter.getExpression(),0);
		    		if (filter.length()>0)
		    			uriParams = appendPath(uriParams, "$filter=" + Encoder.encode(filter).replace("+", "%20"));
	    		}
	    		uriParams = appendPath(uriParams, getSortTermsQueryParam(_queryFilter.getSort()));
	    	}    		
  	
	       	uriParams = appendPath(uriParams, input.getDynamicProperties().get("extraURIParams"));
	       	if (this.getPageSizeQueryParam()!=null)      		
	       		uriParams = appendPath(uriParams, this.getPageSizeQueryParam()+"=" + _pageSize);
	       	String fullpath = path + uriParams;
	       	httpResponse = this.getConnection().doExecute(fullpath, null, "GET");
	        long numDocuments = 0; 
	        long numInPage;
	        
        	JSONResponseSplitter respSplitter=null;
	        do {   
            	numInPage=0;
                InputStream is = null;
                try {
                	if (numDocuments>0)//get the next page from the API
                	{
                		//get the nextpage
			           	if (skipTokenPath!=null)
			           	{
			           		fullpath = respSplitter.getSkipToken();
			           		logger.info(skipTokenPath + ":" + fullpath);
			           		fullpath=fullpath.substring(getConnection().getBaseUrl().length()); //Hack off the base URL to get just the path
			           	}
			           	else 
			           	{
			           		fullpath=path+uriParams+"&"+this.getNextPageQueryParam(numDocuments);
			           	}
			           	httpResponse = getConnection().doExecute(fullpath, null, "GET");
                	}
                    is = httpResponse.getEntity().getContent();
                    int httpStatusCode = httpResponse.getStatusLine().getStatusCode();
                    String httpStatusMessage = httpResponse.getStatusLine().getReasonPhrase();
                    if (is != null &&httpStatusCode==200) {
//Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name());
//String responseString = scanner.useDelimiter("\\A").next();
//System.out.println(responseString);
//scanner.close();
//is = new ByteArrayInputStream(responseString.getBytes());
                    	respSplitter = new JSONResponseSplitter(is, pageItemPath, skipTokenPath);
                    	for(Payload p : respSplitter) {	
                    		ByteArrayOutputStream tempOutputStreamOut = new ByteArrayOutputStream();
//		                    		OutputStream tempOutputStreamOut = getContext().createTempOutputStream();
                    		ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    		try {
	                    		p.writeTo(bos);
                    		} catch (JsonParseException e)
                    		{
//TODO we can't show details of the bad JSON because the stream was already read, also probably a privacy issue as data might appear	                    			
//		                    			Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name());
//		                    			String responseString = scanner.useDelimiter("\\A").next();
//		                    			scanner.close();
//		                    			int errorLocation=(int)e.getLocation().getColumnNr();
//		                    			//Go backwards to capture the key
//		                    			int problemStart = errorLocation;
//		                    			while (problemStart>=0)
//		                    			{
//		                    				char c = responseString.charAt(problemStart);
//		                    				if (c=='{' || c==',')
//		                    					break;
//		                    				problemStart--;
//		                    			}
//		                    			String badJson = responseString.substring(problemStart, errorLocation);
                    			
                    			throw new ConnectorException("The SAP API returned malformed JSON. REQUEST: " + fullpath + " ERROR: " + e.getMessage()
                    				+ ". Run the query externally to capture the malformed JSON and please contact SAP and provide the full text of this message.");
                    		}
                    		ODataParseUtil.parseODataToBoomi(new ByteArrayInputStream(bos.toByteArray()), tempOutputStreamOut, _operationCookie); 
//		                    		InputStream dataOut = getContext().tempOutputStreamToInputStream(tempOutputStreamOut);
                    		InputStream dataOut = new ByteArrayInputStream(tempOutputStreamOut.toByteArray());
                    		numDocuments++;
			            	numInPage++;
		                    response.addPartialResult(input, OperationStatus.SUCCESS, httpStatusCode+"", httpStatusMessage, ResponseUtil.toPayload(dataOut));
		            		if (_maxDocuments > 0 && numDocuments >= _maxDocuments)
		                		break;
		                }
		                respSplitter.close();
			           	logger.log(Level.INFO, "Document index:" + numDocuments);
                	} else {
//                		error=true;
                		Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name());
                		String responseString = scanner.useDelimiter("\\A").next();
                		scanner.close();
                		httpStatusMessage += " " + responseString;
                		is = new ByteArrayInputStream(responseString.getBytes());
	                    response.addPartialResult(input, OperationStatus.FAILURE, httpStatusCode+"", httpStatusMessage, ResponseUtil.toPayload(is));
	                    throw new ConnectorException(httpStatusCode + "-" + httpStatusMessage);
                	}
//	                    }
//	                    else {
//	                        response.addEmptyResult(input, OperationStatus.FAILURE, httpStatusCode+"", httpStatusMessage);
//	                        error=true;
//	                        break;
//	                    }
                }
                finally {
                    IOUtil.closeQuietly(is);
                    IOUtil.closeQuietly(httpResponse);
                }
        		if (_maxDocuments > 0 && numDocuments >= _maxDocuments)
            		break;
	        } while(hasMore(respSplitter, numInPage));	        
        	response.finishPartialResult(input);
		} catch (Exception e) {
			e.printStackTrace();
            ResponseUtil.addExceptionFailure(response, input, e);
		} 
	}
	
	@Override
    public ODataClientConnection getConnection() {
        return (ODataClientConnection) super.getConnection();
    }
	
	private boolean hasMore(JSONResponseSplitter respSplitter, long numInPage)
	{
		//TODO some may support a hasMore response element but that is redundant for offset pagination which must have a pagesize		
		if (skipTokenPath!=null)
			return respSplitter.getSkipToken()!=null;
		if(getPageSizeQueryParam()!=null && numInPage < _pageSize) //If num returned is less than the page size, we are done
			return false;

		return true;
	}
	
	private static String appendPath(String uriParams, String newParams)
	{
		if (newParams != null && newParams.length()>0)
		{
			if (uriParams==null)
				uriParams = "";
			if (uriParams.length()>0)
				uriParams += "&" + newParams;
			else 
				uriParams = "?" + newParams;
		}
		return uriParams;
	}
	
	//TODO remove redundant nav's and watch for selects that are selected without a nav
    private String getExpandTermsQueryParameter(List<String> selectedFields)
    {
    	List<String> navigations= new ArrayList<String>();
    	Map<String, String> optimizedNavigations= new HashMap<String, String>();
    	StringBuilder expand = new StringBuilder();
    	if (selectedFields!=null)
    	{
        	for (String select : selectedFields)
        	{
        		int lastSlash=select.lastIndexOf("/");
        		if (lastSlash>1)
        		{
        			String navigation = select.substring(0,lastSlash);
            		String key = "/"+navigation;
            		
            		if (_operationCookie.isNavigationProperty(key))
            		{
           				navigations.add(navigation);
            		}
        		}
        	}
    	}
    	
    	//Optimize $expand by leaving the longest navigations
    	for (String navigation1:navigations)
    	{
    		String optimumNavigation = navigation1;
        	for (String navigation2:navigations)
        	{
        		if (!optimumNavigation.contentEquals(navigation2) && navigation2.startsWith(optimumNavigation))
        			optimumNavigation=navigation2;
        	}
        	optimizedNavigations.put(optimumNavigation,optimumNavigation);
    	}
    	
    	for (String navigation:optimizedNavigations.keySet())
    	{
			if (expand.length()>0)
				expand.append(",");
			expand.append(navigation);
    	}
    	
    	if (expand.length()>0)
    		expand.insert(0, "$expand=");
    	return expand.toString();
    }
	
    //TODO ideally we would know total fields so we could exclude select if default is all. We could set a `ie for that
	//TODO this could be moved to abstract class if we key on value of PAGINATION_SELECT_URIPARAM not null? Comma delimited fields seems to be the standard when selection is implemented
    /**
     * Build the API URL query parameter to indicate what fields the user selected in the Fields list in the Query Operation UI
     * Defaults to "fields=x,y,z"
     * @param selectedFields the list of fields the user selected for the query operation
     * @return the field URI parameter for the selection
     */
    protected String getSelectTermsQueryParam(List<String> selectedFields)
    {
    	StringBuilder terms= new StringBuilder();
    	
    	if (selectedFields!=null && selectedFields.size()>0)// && selected.size()!=totalNumberFields)
    	{
        	for (String select : selectedFields)
        	{
				if (terms.length()>0)
					terms.append(",");
				terms.append(select);
    		}
    	}  	
    	if (terms.length()>0)
    		terms.insert(0, "$select=");
    	return terms.toString();
    }
    
	//This default behavior is to build terms as individual query parameters.
	//AND is assumed
    /**
     * Build the Filter path parameter from the filters in the Query Operation UI
     * Defaults to setting individual path parameters for each simple expression with each parameter a term in a top level AND expression
     * @param queryFilter the filter expression specified by the user in the Query Operation Filter UI
     * @return the field URI parameter for the filter
     * @throws IOException 
     */
	protected String getFilterTermsQueryParam(Expression baseExpr, int depth) throws IOException
	{
		String queryParameter="";

         // see if base expression is a single expression or a grouping expression
        if (baseExpr!=null)
        {
        	//A single operator, no AND/OR
            if(baseExpr instanceof SimpleExpression) {                
                // base expression is a single simple expression
            	queryParameter=(buildSimpleExpression((SimpleExpression)baseExpr));
            } else {

                // handle single level of grouped expressions
                GroupingExpression groupExpr = (GroupingExpression)baseExpr;

                // parse all the simple expressions in the group
                for(Expression nestedExpr : groupExpr.getNestedExpressions()) {
                    if(nestedExpr instanceof GroupingExpression) {
                        queryParameter+=getFilterTermsQueryParam(nestedExpr, depth+1);
                    } else {
                        String term=(buildSimpleExpression((SimpleExpression)nestedExpr));
                        if (term!=null && term.length()>0)
                        {
                            if (queryParameter.length()>0)
                            	queryParameter+=" " + groupExpr.getOperator().toString().toLowerCase() + " ";
                            queryParameter+=term;
                        }
                    }
                }
            }
        }       
        if (depth>0)
        	queryParameter = "(" + queryParameter + ")";
//        if (queryParameter.length()>0)
//        	queryParameter=URLEncoder.encode(queryParameter);
        return queryParameter.replace("+", "%20");
	}
    
    /**
     * Override to build a filter expression for the unique grammar of the API.
     * For example create=2021-01-01&createdCompare=lessThan.
     * The default behavior is simple ANDed expressions using the eq operation &email=j@email.com&active=true
     * @param simpleExpression the simple expression from which to construct the uri parameter and value
     * @return the URL query parameter and value for the filter compare expression
     * @throws IOException 
     */
	protected String buildSimpleExpression(SimpleExpression expr) throws IOException {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (propName==null || propName.length()==0)
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));
        String type = _operationCookie.getEdmType("/"+propName);
        //TODO could the parameter every be a parent/navigation property? If so through an error should be thrown
        parameter = ODataEdmType.boomiValuetoODataPredicate(parameter, type);
        switch (operator)
        {
        case "startswith":
        case "endswith":
        	term = operator + "("+propName + "," + " " + parameter+") eq true"; //TODO do we really need eq true?
        	break;
        case "substring":
        	term = operator + "(" + parameter + "," + propName+") eq true";
        	break;
        default:
        	term = propName + " " + operator + " " + parameter+"";
        }

        return term;
    }
    /**
     * Override to build a sort expression for the unique grammar of the API.
     * For example orderby=lastName,firstName
     * @param queryFilter provides the user settings from the Query Operation page
     * @return the URL query parameter and value for the sort expression
     */
    protected String getSortTermsQueryParam(List<Sort> sortTerms)
    {
    	String sortTermsString="";

    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()!=0)
        			{
        				sortTermsString+=",";
        			}
        			sortTermsString += sortTerm;
        			if (sort.getSortOrder()!=null)
        				sortTermsString += " " + sort.getSortOrder();
        		}
        	}
    	}
    	if (sortTermsString.length()!=0)
    		sortTermsString = "$orderby=" + URLEncoder.encode(sortTermsString).replace("+", "%20");
    	return sortTermsString;
    }   
    
   /**
    * Override to specify offset pagination path parameters
    * Defaults to offset=<number of documents queried thus far>
    * Note this is not used if getNextPageURLElementPath is set
    * @param numDocuments the number of documents queried thus far
    * @return the path parameter to indicate the next page to query
    */
	protected String getNextPageQueryParam(long numDocuments)
	{
		return "$skip="+numDocuments;
	}
	
    /**
     * Override to specify the name of the parameter that indicates the page size query parameter to specify the number of records per page
     * Defaults to "limit"
     * @return the page size query parameter name
     */
	protected String getPageSizeQueryParam()
	{
		return "$top";
	}
				
	/**
	 * @return the maximum number of documents set by the user in the query operation page
	*/
    public long getMaxDocuments(PropertyMap opProps)
    {
    	return opProps.getLongProperty(OperationProperties.MAXDOCUMENTS.name(), -1L);
    }
    
	/**
	 * @return the page size set by the user in the query operation page
	*/
    public long getPageSize(PropertyMap opProps)
    {
        return opProps.getLongProperty(OperationProperties.PAGESIZE.name(), 100L);
    }
}
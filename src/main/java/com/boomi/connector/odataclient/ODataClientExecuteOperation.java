package com.boomi.connector.odataclient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Scanner;
import java.util.logging.Logger;
import org.apache.http.client.methods.CloseableHttpResponse;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class ODataClientExecuteOperation extends BaseUpdateOperation {

	Logger logger = Logger.getLogger("ODataClientExecuteOperation");
	protected ODataClientExecuteOperation(ODataClientConnection conn) {
		super(conn);
	}
	//TODO build a $batch from parent and child objects
	//TODO Execute Function Import
	/**
	 * 1. Inbound JSON to Form Encoded parameters...how are things like dates encoded? UTC? DateTimeOffset('xxxxx')????
	 * 2. Do a POST/GET according to m:httpMethod parameter in the metadata
	 * GET <host>/sap/opu/odata/SAP/API_BILL_OF_MATERIAL_SRV;v=2/ExplodeBOM?BillOfMaterial='00058298'&BillOfMaterialCategory='M'&BillOfMaterialVariant='1'&BillOfMaterialVersion=''&EngineeringChangeDocument=''&Material='EX_H'&Plant='0001'&BillOfMaterialItemCategory=''&BOMExplosionApplication='PP01'&BOMExplosionAssembly=''&BOMExplosionDate=datetime'2019-12-07T00%3A00%3A00'&BOMExplosionIsLimited=false&BOMExplosionIsMultilevel=true&BOMExplosionLevel=2.000m&BOMItmQtyIsScrapRelevant=''&MaterialProvisionFltrType=' '&RequiredQuantity=60.000m&SparePartFltrType=' '
<?xml version="1.0" encoding="UTF-8"?>

-<d:ExplodeBOM xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices">


-<d:element m:type="API_BILL_OF_MATERIAL_SRV.DBomheaderforexplosionOut">

<d:bill_of_material_base_quant>300.000</d:bill_of_material_base_quant>

<d:bill_of_material_component>EX_H2</d:bill_of_material_component>

	 * 3. Response could be a complex type, collection or even an entitytype?
	 */
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		Logger logger = response.getLogger();
		getConnection().setLogger(logger);
    	String customOperation = getContext().getCustomOperationType();
    	OperationCookie inputCookie = new OperationCookie(getContext().getObjectDefinitionCookie(ObjectDefinitionRole.INPUT));
		OperationCookie outputCookie = null;
		if ("GET".contentEquals(customOperation) || "POST".contentEquals(customOperation) || "EXECUTE".contentEquals(customOperation))
		{
			outputCookie = new OperationCookie(getContext().getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT));
		}
		PropertyMap opProps = this.getContext().getOperationProperties();
		boolean captureHeaders = opProps.getBooleanProperty("FETCH_HEADERS", false);
		
		//If Capture Headers is turned on, For POST we only do this once to get x-csrf-token and session cookies, not for each document
		if (captureHeaders && "POST".contentEquals(customOperation))
		{
			getSessionHeaders();
		}
				
        String path = getContext().getObjectTypeId();
		for (ObjectData input : request) {
            try {
            	//POST, GET, PUT, PATCH, DELETE, EXECUTE
            	InputStream dataIn = input.getData();
            	OutputStream tempOutputStreamIn = null;
            	InputStream dataOut = null;
            	OutputStream tempOutputStreamOut = null;
        		CloseableHttpResponse httpResponse = null;
                try {           	
	            	boolean hasInputBody=false;
	            	//GET, DELETE have only key values as predicate keys, nothing to send to api server
	            	//EXECUTE input get converted to url parameters so it has no input body
	            	if ("POST".contentEquals(customOperation) || "PUT".contentEquals(customOperation) || "PATCH".contentEquals(customOperation))
	            	{
	            		hasInputBody=true;
                		//TODO I can't get createTempOutputStream work....same issues as in ODataClientQueryOperation
//	            		tempOutputStreamIn = getContext().createTempOutputStream();
	            		tempOutputStreamIn = new ByteArrayOutputStream();
	            	} 
	            	String httpMethod = customOperation;
	            	String predicate = null;
	            	String resolvedPath = path;
	            	if ("EXECUTE".contentEquals(customOperation))
	            	{
	            		//GET <host>/sap/opu/odata/SAP/API_BILL_OF_MATERIAL_SRV;v=2/ExplodeBOM?BillOfMaterial='00058298'&BillOfMaterialCategory='M'&BillOfMaterialVariant='1'&BillOfMaterialVersion=''&EngineeringChangeDocument=''&Material='EX_H'&Plant='0001'&BillOfMaterialItemCategory=''&BOMExplosionApplication='PP01'&BOMExplosionAssembly=''&BOMExplosionDate=datetime'2019-12-07T00%3A00%3A00'&BOMExplosionIsLimited=false&BOMExplosionIsMultilevel=true&BOMExplosionLevel=2.000m&BOMItmQtyIsScrapRelevant=''&MaterialProvisionFltrType=' '&RequiredQuantity=60.000m&SparePartFltrType=' '
	            		//	Content-Type: multipart/mixed; boundary=batch
//	            		POST <host>>/sap/opu/odata/SAP/API_BILL_OF_MATERIAL_SRV;v=2/DeleteBOMHeaderWithECN?BillOfMaterial='00048145'&BillOfMaterialCategory='M'&BillOfMaterialVariant='1'&BillOfMaterialVersion=''&EngineeringChangeDocument=''&Material='AA_BDL_API_TEST'&Plant='0001'&EngineeringChangeDocForEdit='TEST_ECN'
//	            			HTTP Request Headers
//	            			Content-Type: application/json; charset=utf-8
//	            			If-Match: (ETAG value � LastChangeDateTime)
	            		resolvedPath = path + "?" + ODataParseUtil.parseBoomiToFunctionImportURL(dataIn, inputCookie);
	            		httpMethod=inputCookie.getHttpMethod();
	            	} else {
		            	//Note predicate ignored for POST
		            	predicate = ODataParseUtil.parseBoomiToOData(dataIn, tempOutputStreamIn, inputCookie);	            		
	            	}
	            	if (hasInputBody)
	            	{
                		//TODO I can't get createTempOutputStream work....same issues as in ODataClientQueryOperation
//	            		dataIn = getContext().tempOutputStreamToInputStream(tempOutputStreamIn);
	            		dataIn = new ByteArrayInputStream(((ByteArrayOutputStream)tempOutputStreamIn).toByteArray());
	            	}
	            	else
	            		dataIn=null;
	            	
	            	if (!"POST".contentEquals(customOperation) && !"EXECUTE".contentEquals(customOperation))
	            	{
	            		if (StringUtil.isEmpty(predicate))
	            			throw new ConnectorException("A predicate key is required for this operation: " + customOperation);
	            		resolvedPath+=predicate;	
	            		//If capture headers is turned on, For for DELETE, PUT, POST we will do GET to get the etag for each inbound document
	            		if (captureHeaders && !"GET".contentEquals(customOperation))
	            		{
	            			getETagHeaders(resolvedPath, inputCookie);
	            		}
	            	}
                	httpResponse = getConnection().doExecute(resolvedPath, input, httpMethod, !captureHeaders);

	            	//If a GET capture the headers into the document properties
                	PayloadMetadata payloadMetadata = null;
                	if ("GET".contentEquals(httpMethod))
                		payloadMetadata = getConnection().setSecurityHeaderDocumentProperties(getContext());
                	
                	if (httpResponse.getEntity()!=null)
                		dataOut = httpResponse.getEntity().getContent();
                    
                	OperationStatus status = OperationStatus.SUCCESS;
                    int httpResponseCode = httpResponse.getStatusLine().getStatusCode();
                    String statusMessage = httpResponse.getStatusLine().getReasonPhrase();
                    if (httpResponseCode>=300)
                    	status = OperationStatus.FAILURE;
                    
                    if (dataOut != null) {
                    	//Only attempt to parse response if OK
                    	if (status == OperationStatus.SUCCESS)
                    	{
                    		//TODO I can't get createTempOutputStream work....same issues as in ODataClientQueryOperation
//                    		tempOutputStreamOut = getContext().createTempOutputStream();
                    		tempOutputStreamOut = new ByteArrayOutputStream();
                        	ODataParseUtil.parseODataToBoomi(dataOut, tempOutputStreamOut, outputCookie);
//                    		dataOut = getContext().tempOutputStreamToInputStream(tempOutputStreamOut);
                       		dataOut = new ByteArrayInputStream(((ByteArrayOutputStream)tempOutputStreamOut).toByteArray());
                    	} else {
                    		//For errors we want to append the stream contents so that it shows in shape exception.
                    		//TODO do we want to scrap this and require looking in the log?
                    		Scanner scanner = new Scanner(dataOut, StandardCharsets.UTF_8.name());
                    		String responseString = scanner.useDelimiter("\\A").next();
                    		scanner.close();
                    		statusMessage += " " + responseString;
                    		dataOut = new ByteArrayInputStream(responseString.getBytes());
                    	}
                        response.addResult(input, status, httpResponseCode+"",
                        		statusMessage, PayloadUtil.toPayload(dataOut, payloadMetadata));
                    }
                    else {
                        response.addEmptyResult(input, status, httpResponseCode+"", statusMessage);
                    }
                }
                finally {
                    IOUtil.closeQuietly(dataIn);
                    IOUtil.closeQuietly(tempOutputStreamIn);
                    IOUtil.closeQuietly(httpResponse);
                    IOUtil.closeQuietly(dataOut);
                    IOUtil.closeQuietly(tempOutputStreamOut);
                }
            }
            catch (Exception e) {
                // make best effort to process every input
            	e.printStackTrace();
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
	}

	/*
	 * Execute a GET on the service root to pull in the session cookies and CSRF Token. 
	 * This is required for POST operations.
	 * The cookie and token are persisted in the ODataCLientConnection object
	 */
	private void getSessionHeaders()
	{
		CloseableHttpResponse httpResponse=null;
		//GET on the service URL to capture the session cookies and xcsrf. Note that our GET operations always set x-csrf-token to fetch for each request
		//The headers are captured in the connection object
    	try {
			httpResponse = getConnection().doExecute("", null, "GET");
		} catch (IOException | GeneralSecurityException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(httpResponse);
		}
	}
	
	/*
	 * Execute a GET on the predicate key to pull in the etag, session cookies and CSRF Token. 
	 * This is required for PATCH, PUT and DELETE operations.
	 * The etag, cookie and token are persisted in the ODataCLientConnection object. 
	 * Note if the eTag is not returned in the header, an attempt to pull from the response payload will occur.
	 */
	private void getETagHeaders(String resolvedPath, OperationCookie inputCookie)
	{
		CloseableHttpResponse httpResponse=null;
		InputStream dataOut=null;
		//GET on the service URL to capture the session cookies and xcsrf. Note that our GET operations always set x-csrf-token to fetch for each request
		//The headers are captured in the connection object
    	try {
			httpResponse = getConnection().doExecute(resolvedPath, null, "GET");
			dataOut = httpResponse.getEntity().getContent();
			if (StringUtil.isNotEmpty(getConnection().geteTag()))
			{
            	String eTag = ODataParseUtil.parseODataToBoomi(dataOut, null, inputCookie);
            	if (StringUtil.isNotBlank(eTag))
            	{
            		logger.info("ETag found in GET response body: " + eTag);
            		getConnection().seteTag(eTag);
            	}
			}
		} catch (IOException | GeneralSecurityException e) {
			throw new ConnectorException(e);
		} finally {
			IOUtil.closeQuietly(dataOut);
			IOUtil.closeQuietly(httpResponse);
 		}
	}

	@Override
	public ODataClientConnection getConnection() {
		return (ODataClientConnection) super.getConnection();
	}
}
package com.boomi.connector.odataclient;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.edm.EdmAnnotationAttribute;
import org.apache.olingo.odata2.api.edm.EdmAnnotations;
import org.apache.olingo.odata2.api.edm.EdmComplexType;
import org.apache.olingo.odata2.api.edm.EdmEntityContainer;
import org.apache.olingo.odata2.api.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.edm.EdmEntityType;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmFunctionImport;
import org.apache.olingo.odata2.api.edm.EdmMultiplicity;
import org.apache.olingo.odata2.api.edm.EdmNavigationProperty;
import org.apache.olingo.odata2.api.edm.EdmParameter;
import org.apache.olingo.odata2.api.edm.EdmProperty;
import org.apache.olingo.odata2.api.edm.EdmTyped;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.json.JSONException;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.IOUtil;

public class ODataClientBrowser extends BaseBrowser  implements ConnectionTester {
	public static final String KEYPARAMETERSROOTNAME = "___KEYS";
	Logger logger;
	long _maxBrowseDepth=0;
	boolean _includeRecursiveNavigations = false;
	List<String> visitedTypes = new ArrayList<String>();
	private PropertyMap opProps;
    public ODataClientBrowser(ODataClientConnection conn) {
        super(conn);
        logger = Logger.getLogger(this.getClass().getName());
        opProps = this.getContext().getOperationProperties();
        _includeRecursiveNavigations = opProps.getBooleanProperty("INCLUDE_RECURSIVE_NAVIGATIONS", false);
    }

	@Override
	public ObjectTypes getObjectTypes() 
	{
		ObjectTypes objectTypes = new ObjectTypes();		
//		client.getConfiguration().setHttpClientFactory(new BasicAuthHttpClientFactory("BOOMI_INTEGRATION", "xXstSNlvWdxGFpSraNcCfLqhuxrzKHUEtTgoV+X9"));
//		 Edm edm = client.getRetrieveRequestFactory().getMetadataRequest("https://my304976-api.s4hana.ondemand.com:443/sap/opu/odata/sap/MD_SUPPLIER_MASTER_SRV/").execute().getBody();
//		getConnection().doExecute(path, httpMethod, null)

		try {
			//TODO deletable etc?
			//TODO Function imports
			Edm edm=getEdm();
			EdmEntityContainer edmEntityContainer = edm.getDefaultEntityContainer();
			if (getContext().getOperationType()==OperationType.EXECUTE && "EXECUTE".contentEquals(getContext().getCustomOperationType()))
			{
				//Function Import
				for (EdmFunctionImport functionImport : edm.getFunctionImports())
				{
					ObjectType objectType = new ObjectType();
					objectType.setHelpText(functionImport.getName());
					objectType.setId(functionImport.getName()); 
					objectType.setLabel(functionImport.getName());
					objectTypes.getTypes().add(objectType);					
				}
			} else {
				for (EdmEntitySet entitySet:edmEntityContainer.getEntitySets())
				{
					EdmEntityType entityType = entitySet.getEntityType();
					String entitySetName=entitySet.getName();
					
					String label = entitySetName;
					EdmAnnotations annotations = entityType.getAnnotations();
					List<EdmAnnotationAttribute> attributes = annotations.getAnnotationAttributes();
					String typeLabel=null;
					String typeQuickInfo=null;
					if (attributes!=null)
					{
						for (EdmAnnotationAttribute attribute : attributes)
						{
							switch (attribute.getName())
							{
							case "quickinfo":
								typeQuickInfo=attribute.getText();
								break;
							case "label":
								typeLabel=attribute.getText();
								break;
							}
						}
					}
					
					if (typeQuickInfo!=null && typeQuickInfo.length()>0)
						label += " - " + typeQuickInfo;
					else if (typeLabel!=null && typeLabel.length()>0)
						label += " - " + typeLabel;
					ObjectType objectType = new ObjectType();
					objectType.setHelpText(label);
					objectType.setId(entitySetName); 
					objectType.setLabel(label);
					objectTypes.getTypes().add(objectType);
				}
//				SwaggerBrowseUtil.sortObjectTypes(objectTypes);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ConnectorException(e);
		}	
		return objectTypes;
	}
	
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)		
	{ 
        this._maxBrowseDepth = getConnection().getMaxBrowseDepth(getContext().getOperationProperties());

		ObjectDefinitions objDefinitions = new ObjectDefinitions();
		OperationType operationType = getContext().getOperationType();
		String customOperationType = getContext().getCustomOperationType();
		try {
			Edm edm = getEdm();
			if (getContext().getOperationType()==OperationType.EXECUTE && "EXECUTE".contentEquals(customOperationType))
			{
				//Function Import
				//TODO json input type, to be converted to query params
//<FunctionImport Name="C_BPAddlSuplrDunningPreparation" 
//ReturnType="MD_SUPPLIER_MASTER_SRV.C_BPAddlSuplrDunningType" 
//EntitySet="C_BPAddlSuplrDunning" m:HttpMethod="POST" sap:action-for="MD_SUPPLIER_MASTER_SRV.C_BPAddlSuplrDunningType" sap:applicable-path="Preparation_ac">
//<Parameter Name="BusinessPartner" Type="Edm.String" Mode="In" MaxLength="10"/>
//<Parameter Name="Supplier" Type="Edm.String" Mode="In" MaxLength="10"/>

				EdmFunctionImport functionImport = edm.getDefaultEntityContainer().getFunctionImport(objectTypeId);
				for(ObjectDefinitionRole role : roles)
				{
					ObjectDefinition objDefinition = new ObjectDefinition();
					OperationCookie operationCookie = new OperationCookie();
					if (ObjectDefinitionRole.INPUT == role)
					{
						objDefinition.setInputType(ContentType.JSON);
						String requestSchema = this.getFunctionInputSchema(functionImport, operationCookie);
						objDefinition.setElementName("/"+functionImport.getName()); 
						objDefinition.setJsonSchema(requestSchema);
					} 					
					else if (ObjectDefinitionRole.OUTPUT == role)
					{
						objDefinition.setOutputType(ContentType.JSON);
						String responseSchema = this.getFunctionOutputSchema(edm, functionImport, operationCookie);
						objDefinition.setElementName("/"+functionImport.getReturnType().getType().getName()); 
						objDefinition.setJsonSchema(responseSchema);
					}
					objDefinitions.getDefinitions().add(objDefinition);								
					objDefinition.setCookie(operationCookie.toString());
				}
			} else {
				EdmEntityContainer edmEntityContainer = edm.getDefaultEntityContainer();
				EdmEntitySet entitySet = edmEntityContainer.getEntitySet(objectTypeId);
				if (entitySet==null)
					throw new Exception("Entity type not found in the OData entity set:" + objectTypeId);
				EdmEntityType edmEntityType = entitySet.getEntityType();
				for(ObjectDefinitionRole role : roles)
				{
					ObjectDefinition objDefinition = new ObjectDefinition();
					OperationCookie operationCookie = new OperationCookie();
					if (ObjectDefinitionRole.INPUT == role)
					{
						objDefinition.setInputType(ContentType.JSON);
						switch (operationType)
						{
						case EXECUTE:
							switch (customOperationType)
							{
							case "GET":
							case "DELETE":
								//Both DELETE and GET require path parameters
								String requestSchema = this.getKeysRequestProfileSchema(edmEntityType, operationCookie);
								objDefinition.setElementName("/"+KEYPARAMETERSROOTNAME); 
								objDefinition.setJsonSchema(requestSchema);
								break;
							case "POST":
							case "PUT":
							case "PATCH":
								objDefinition.setElementName("/"+objectTypeId); 							
								objDefinition.setJsonSchema(getEntityTypeSchema(edm, edmEntityType, objectTypeId, operationCookie));
								break;
							}
							objDefinitions.getDefinitions().add(objDefinition);								
							break;
						default:
							break;
						}	
					} 
					else if (ObjectDefinitionRole.OUTPUT == role)
					{
						switch (operationType)
						{
						case EXECUTE:
							//PUT, MERGE, DELETE have no response
							//EXECUTE has FI response
							//GET drops down to next case
							if(!"GET".contentEquals(customOperationType) && !"POST".contentEquals(customOperationType))
								break;
						case QUERY:
							//TODO do we do fieldspec fields? Or just leave them all filterable and sortable?
							objDefinition.setOutputType(ContentType.JSON);
							objDefinition.setElementName("/"+objectTypeId); //specify root element as schema location
							objDefinition.setJsonSchema(getEntityTypeSchema(edm, edmEntityType, objectTypeId, operationCookie));
							objDefinitions.getDefinitions().add(objDefinition);								
							break;
						default:
							break;
						}
					}
					objDefinition.setCookie(operationCookie.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ConnectorException(e);
		}
		return objDefinitions;
	}
	
	private String getFunctionInputSchema(EdmFunctionImport functionImport, OperationCookie operationCookie) throws EdmException {
		JSONObject root = new JSONObject();
		root.put("$schema", "http://json-schema.org/draft-04/schema#");
		JSONObject schemaObj = new JSONObject();
		root.put(functionImport.getName() , schemaObj);
		schemaObj.put("type", "object");
		JSONObject properties = new JSONObject();
		schemaObj.put("properties" , properties);
		for(String name : functionImport.getParameterNames())
		{
			EdmParameter parameter = functionImport.getParameter(name);
			JSONObject property = new JSONObject();
			properties.put(name, property);				
			ODataEdmType.buildSchemaType(parameter.getType().getName(), property);
			if (!parameter.getType().getName().contentEquals("String")) //We are default to String to minimize the size of the cookie under the assumption that String is most prevalent
				operationCookie.addProperty(parameter.getType().getName(), "", parameter.getName());
		}
		operationCookie.setHttpMethod(functionImport.getHttpMethod());
		return root.toString();
	}

	public String getKeysRequestProfileSchema(EdmEntityType edmEntityType, OperationCookie operationCookie) throws EdmException, JSONException
	{
		JSONObject root = new JSONObject();
		root.put("$schema", "http://json-schema.org/draft-04/schema#");
		JSONObject schemaObj = new JSONObject();
		root.put(KEYPARAMETERSROOTNAME , schemaObj);
		schemaObj.put("type", "object");
		JSONObject properties = new JSONObject();
		schemaObj.put("properties" , properties);
		edmEntityType.getKeyProperties();
		for (EdmProperty keyProperty:edmEntityType.getKeyProperties())
		{
			JSONObject property = new JSONObject();
			properties.put(keyProperty.getName(), property);
			ODataEdmType.buildSchemaProperty(keyProperty, property);
			operationCookie.addKey(keyProperty.getType().getName(), "", keyProperty.getName());
		}
		return root.toString();
	}

	private String getFunctionOutputSchema(Edm edm, EdmFunctionImport functionImport, OperationCookie operationCookie) throws EdmException {
		JSONObject root = new JSONObject();
		root.put("$schema", "http://json-schema.org/draft-04/schema#");
		JSONObject schemaObj = new JSONObject();
		root.put(functionImport.getReturnType().getType().getName() , schemaObj);
		JSONObject properties = new JSONObject();
		
		//Collections
		if (functionImport.getReturnType().getMultiplicity()==EdmMultiplicity.MANY)
		{
			schemaObj.put("type", "array");
			JSONObject arrayItems = new JSONObject();
			schemaObj.put("items", arrayItems);
			arrayItems.put("type", "object");
			arrayItems.put("properties" , properties);
		} else {
			schemaObj.put("type", "object");
			schemaObj.put("properties" , properties);
		}
		EdmTyped type = functionImport.getReturnType();
		
			EdmComplexType complexType = edm.getComplexType(type.getType().getNamespace(),type.getType().getName());
			for (String propertyName : complexType.getPropertyNames())
			{
				EdmTyped typed = complexType.getProperty(propertyName);
				if (typed instanceof EdmProperty)
				{
					EdmProperty propType = (EdmProperty) typed;
					JSONObject property = new JSONObject();
					properties.put(propertyName, property);				
					ODataEdmType.buildSchemaProperty(propType, property);
					if (!propType.getType().getName().contentEquals("String")) //We are default to String to minimize the size of the cookie under the assumption that String is most prevalent
						operationCookie.addProperty(propType.getType().getName(), "", propertyName);
				} 
//					else {
//					//TODO ComplexTypes require recursive call, for now they will throw an exception 
//				}
			}		
		return root.toString();
	}

	private String getEntityTypeSchema(Edm edm, EdmEntityType edmEntityType, String objectTypeId, OperationCookie operationCookie) throws EdmException
	{
		JSONObject schema = new JSONObject();
		schema.put("$schema", "http://json-schema.org/schema#");
		JSONObject root = new JSONObject();
		schema.put(objectTypeId, root);
		getEntityTypeSchemaRecursive(edm, edmEntityType, root, 0, "", operationCookie, null);
		return schema.toString();
	}
	
	//<Property Name="BusinessPartner" Type="Edm.String" Nullable="false" MaxLength="10" sap:display-format="UpperCase" sap:label="Business Partner" sap:quickinfo="Business Partner Number"/>
	//<Property Name="DraftEntityCreationDateTime" Type="Edm.DateTimeOffset" Precision="7" sap:label="Draft Created On" sap:heading="" sap:quickinfo="" sap:creatable="false" sap:updatable="false"/>
	//<Property Name="WithholdingTaxExmptPercent" Type="Edm.Decimal" Precision="5" Scale="2" sap:label="Exemption rate"/>
	//TODO description=quickinfo, maxlength=maxlenght, nullable
	private void getEntityTypeSchemaRecursive(Edm edm, EdmEntityType edmEntityType, JSONObject root, long depth, String path, OperationCookie operationCookie, EdmEntityType parentEdmEntityType) throws EdmException
	{
		root.put("title", edmEntityType.getName());
		root.put("type", "object");
		JSONObject properties = new JSONObject();
		root.put("properties", properties);
		for (String propertyName : edmEntityType.getPropertyNames())
		{
			EdmTyped typed = edmEntityType.getProperty(propertyName);
			if (typed instanceof EdmProperty)
			{
				EdmProperty type = (EdmProperty) typed;
				JSONObject property = new JSONObject();
				properties.put(propertyName, property);				
				ODataEdmType.buildSchemaProperty(type, property);
				if (!type.getType().getName().contentEquals("String")) //We are default to String to minimize the size of the cookie under the assumption that String is most prevalent
					operationCookie.addProperty(type.getType().getName(), path, propertyName);
			} 
//				else {
//				//TODO ComplexTypes require recursive call, for now they will throw an exception 
//			}
		}		
		for (EdmProperty edmKeyProperty : edmEntityType.getKeyProperties())
		{
			operationCookie.addKey(edmKeyProperty.getType().getName(), path, edmKeyProperty.getName());
		}
		if (depth<_maxBrowseDepth)
		{
			for (String navigationPropertyName : edmEntityType.getNavigationPropertyNames())
			{
				EdmNavigationProperty type = (EdmNavigationProperty)edmEntityType.getProperty(navigationPropertyName);
				EdmEntityType childEntityType = type.getRelationship().getEnd1().getEntityType();
				String toRole = type.getToRole();
				if (toRole.contentEquals(type.getRelationship().getEnd2().getRole()))
				{
					childEntityType = type.getRelationship().getEnd2().getEntityType();
				}
				if (childEntityType==null)
					throw new ConnectorException("Child Entity not found for navigation property: " + navigationPropertyName);

				//Don't allow references back to parentEdmEntityType
				String childEntityTypeName = childEntityType.toString();
				if (_includeRecursiveNavigations || parentEdmEntityType==null || (!childEntityTypeName.contentEquals(parentEdmEntityType.toString()) && !visitedTypes.contains(childEntityTypeName)))
				{					
					visitedTypes.add(childEntityTypeName);					
					
					JSONObject newRoot = new JSONObject();
					EdmMultiplicity multiplicity = type.getMultiplicity();
					if (multiplicity==EdmMultiplicity.MANY)
					{
						JSONObject array = new JSONObject();
						properties.put(navigationPropertyName, array);
						//JSONArray
						array.put("type", "array");
						array.put("items", newRoot);
					} else {
						//JSONObject
						properties.put(navigationPropertyName, newRoot);
					}	
					String newPath = path+"/"+navigationPropertyName;
					operationCookie.addNavigationProperty(childEntityTypeName, path, navigationPropertyName, multiplicity==EdmMultiplicity.MANY);
					getEntityTypeSchemaRecursive(edm, childEntityType, newRoot, depth+1, newPath, operationCookie, edmEntityType);
				} else {
					this.logger.info("Recursive navigation excluded: " + path + "/" + childEntityTypeName);
				}
			}
		}
	}
		
	private Edm getEdm() throws JSONException, EntityProviderException, UnsupportedOperationException, IOException, GeneralSecurityException, InterruptedException
	{
		Edm edm=null;
		CloseableHttpResponse response=null;
		try {
			this.getConnection().setAcceptHeader("application/xml");
			response = this.getConnection().doExecute("$metadata", null, "GET");
//			InputStream metadataXml = this.getClass().getResourceAsStream("/resources/metadata.xml");
//			metadataXml = new FileInputStream(new File("src/test/java/resources/northwindv2metadata.xml"));
//			metadataXml = new FileInputStream(new File("src/test/java/resources/metadata.xml"));
			if (response.getStatusLine().getStatusCode()!=200)
				throw new ConnectorException(String.format("Error reading metadata %d %s", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()));
			edm = EntityProvider.readMetadata(response.getEntity().getContent(), false);
		} finally {
			//TODO why does closequietly impact junit tests different than just close?
//			Thread.sleep(5000);
			IOUtil.closeQuietly(response);
//			if (response!=null)
//				response.close();
		}
		return edm;
	}
	
	@Override
	public void testConnection() {
		try {
			this.getConnection().testConnection();
        }
        catch (Exception e) {
            throw new ConnectorException("Could not establish a connection", e);
        }
	}

	@Override
    public ODataClientConnection getConnection() {
        return (ODataClientConnection) super.getConnection();
    }
}
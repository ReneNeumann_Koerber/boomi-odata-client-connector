package com.boomi.connector.odataclient;

import org.apache.olingo.odata2.api.edm.EdmException;
import org.json.JSONException;
import org.json.JSONObject;

import com.boomi.connector.api.ConnectorException;

public class OperationCookie {
	
	private JSONObject _properties;
	private JSONObject _cookie;
	private String _httpMethod="GET"; //only used for Function Import request

	OperationCookie()
	{
		_cookie = new JSONObject();	
		_properties=new JSONObject();
		_cookie.put("properties", _properties);
	}
	
	OperationCookie(String cookieString)
	{
		if (cookieString==null || cookieString.length()==0)
			throw new ConnectorException("Operation cookie is null, please reimport the operation");		
		_cookie = new JSONObject(cookieString);	
		_properties = _cookie.getJSONObject("properties");
		if (_cookie.has("httpMethod"))
			_httpMethod = _cookie.getString("httpMethod");
	}
	
	/**
	 * Returns true if the property identified by the name/path is an OData key for the entity
	 * @param key
	 * @return returns true if it is a key
	 */
	public boolean isKey(String key)
	{
		if (_properties.has(key))
		{
			if (_properties.getJSONObject(key).has("isKey")) //its a nav prop
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param cookieKey
	 * @return
	 */
	public String getEdmType(String cookieKey) {
		if (_properties.has(cookieKey))
		{
			JSONObject cookieEntry = _properties.getJSONObject(cookieKey);
			return cookieEntry.getString("type");
		}
		return "String"; //We default to string to minimize cookie size
	}
	
	/**
	 * 
	 * @param edmTypeName
	 * @param path the path of the property within the json profile structure
	 * @param propertyName
	 * @param isArray For navigation properties indicating if it is a 1..* many multiplicity
	 * @throws EdmException
	 * @throws JSONException
	 */
	void addNavigationProperty(String edmTypeName, String path, String propertyName, boolean isArray)
	{
		JSONObject cookieElement = this.createCookieElement(edmTypeName, path, propertyName);
		cookieElement.put("isNavigationProperty", true);
		if (isArray)
			cookieElement.put("isArray", true);
	}
	
	/**
	 * 
	 * @param edmTypeName
	 * @param path the path of the property within the json profile structure
	 * @param propertyName
	 * @throws EdmException
	 * @throws JSONException
	 */
	void addKey(String edmTypeName, String path, String propertyName)
	{
		JSONObject cookieElement = this.createCookieElement(edmTypeName, path, propertyName);
		cookieElement.put("isKey", true);
	}
	
	/**
	 * 
	 * @param edmTypeName
	 * @param path the path of the property within the json profile structure
	 * @param propertyName
	 * @throws EdmException
	 * @throws JSONException
	 */
	void addProperty(String edmTypeName, String path, String propertyName) throws EdmException, JSONException
	{
		createCookieElement(edmTypeName, path, propertyName);
	}
	
	private JSONObject createCookieElement(String edmTypeName, String path, String propertyName)
	{
		JSONObject cookieElement = new JSONObject();
		_properties.put(path+"/"+propertyName, cookieElement);
		cookieElement.put("type", edmTypeName);
		return cookieElement;
	}
	
	@Override
	public String toString()
	{
		return _cookie.toString();
	}

	public boolean isNavigationProperty(String key) {
		if (_properties.has(key))
		{
			if (_properties.getJSONObject(key).has("isNavigationProperty")) //its a nav prop
			{
				return true;
			}
		}
		return false;
	}

	public String getHttpMethod() {
		return _httpMethod;
	}

	public void setHttpMethod(String _httpMethod) {
		this._httpMethod = _httpMethod;
	}
}

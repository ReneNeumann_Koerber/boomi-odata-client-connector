package com.boomi.connector.odataclient;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OAuth2Context;
import com.boomi.connector.api.OAuth2Token;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class ODataClientConnection extends BaseConnection {
    public enum ConnectionProperties {URL, URLALT, SERVICEPATH, AUTHTYPE, USERNAME, PASSWORD, MAXDOCUMENTS, PAGESIZE, OAUTHOPTIONS, MAXBROWSEDEPTH}
	
	public enum AuthType {
		NONE,BASIC,OAUTH,CUSTOM
	}  
	
    private AuthType _authenticationType=AuthType.NONE;
    private final String _username;
    private final String _password;
    protected final PropertyMap _connProps;
    private String acceptHeader = "application/json";
    private String xCSRFToken = null;
    private String sessionCookies = null;
    private String eTag = null;
    
    protected Logger logger = Logger.getLogger(this.getClass().getName());
    
	public ODataClientConnection(BrowseContext context) {
		super(context);
		
		_connProps = context.getConnectionProperties();
        _authenticationType = AuthType.valueOf(_connProps.getProperty(ConnectionProperties.AUTHTYPE.name(), AuthType.NONE.name()));
        _username = _connProps.getProperty(ConnectionProperties.USERNAME.name());
        _password = _connProps.getProperty(ConnectionProperties.PASSWORD.name());
	}
	
    public CloseableHttpResponse doExecute(String path, ObjectData input, String httpMethod, boolean readHeadersFromDPs) throws IOException, GeneralSecurityException {
    	return this.doExecute(path, input, httpMethod, this.getAuthenticationType(), readHeadersFromDPs);
    }

    public CloseableHttpResponse doExecute(String path, ObjectData input, String httpMethod) throws IOException, GeneralSecurityException {
    	return this.doExecute(path, input, httpMethod, this.getAuthenticationType(), false);
    }

	public CloseableHttpResponse doExecute(String path, ObjectData input, String httpMethod, AuthType authType, boolean readHeadersFromDPs)
			throws IOException, GeneralSecurityException {
    	InputStream dataIn = null;
    	if (input!=null)
    		dataIn = input.getData();
		
		String url = getBaseUrl() + path;
   		logger.log(Level.INFO, String.format("Executing %s : %s", httpMethod, url));
		
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;

		try {
				
							
	//		PoolStats poolStats = HTTPClientConnector.GLOBAL_CONNECTION_MANAGER.getTotalStats();
	//		logger.info(String.format("Reusing PoolingHttpClientConnectionManager Available: %d Leased: %d, Pending: %d Max: %d"
	//				,poolStats.getAvailable(), poolStats.getLeased(), poolStats.getPending(), poolStats.getMax()));
			
			httpClient = HttpClients.custom().setConnectionManager(ODataClientConnector.GLOBAL_CONNECTION_MANAGER).build();
			HttpRequestBase httpRequest = null;
			switch (httpMethod) {
			case "DELETE":
				httpRequest = new HttpDelete(url);
				break;
			case "GET":
				httpRequest = new HttpGet(url);
				break;
			case "POST":
				httpRequest = new HttpPost(url);
				break;
			case "PATCH":
				httpRequest = new HttpPatch(url);
				break;
			case "PUT":
				httpRequest = new HttpPut(url);
				break;
			}
			
			// add request headers
			httpRequest.addHeader("Accept", this.getAcceptHeader());
//			httpRequest.addHeader("APIKey", "gW5Ph6q0KsfnfAfVSWGuUGdGkC3pu4qe");
//			httpRequest.addHeader("Cookie", "sap-usercontext=sap-client=100; oucfarwteuvouyrqoreebozzaqoutrvfzedebat=GET%23MIIC0QYJKoZIhvcNAQcDoIICwjCCAr4CAQAxggIYMIICFAIBADB8MHAxCzAJBgNVBAYTAkRFMRwwGgYDVQQKExNTQVAgVHJ1c3QgQ29tbXVuaXR5MRMwEQYDVQQLEwpTQVAgV2ViIEFTMRQwEgYDVQQLEwtJMDAyMDg0ODU2NTEYMBYGA1UEAwwPU1lWX1NTRkFfUzJTVlBFAggKICACJwlHATANBgkqhkiG9w0BAQEFAASCAYCFQq1XMaEL5PBEax39MyBRjPnTeIsmMkhyYe%2bLkkbb5KIGtoT1Qr8ajru8cpk6TwYjC5T4SC9yuxhnSOXaZhl3D8Q5VOPbpez3FEs%2fWlgzJMfk3Rhf3K6%2frQCP3P6b%2fWDugrpzjg0LMOteHiCJR7JQrqpZ07JFuGmTx3e%2bocLHOrqstTZmVTLQw7mTKW4YM2fpHokde%2bVuQg6ienRXM9qPB0DCQLZsNmkf3RqusNUmB3UZQOynxi7fcTslcEg9PNazXPk92Q%2ff%2bjoS1njD8j7x02mu5jI5fS9ZVdTNKymRtNYfZKohsiRoo9Nk5XLpgca0iWZfoVdUvLXfxz5Ho8QJOl4qkc4YCAwLaxv31S4bPQItjixRD%2far%2fOQY1atordzTh83q1sx0YI1GY%2bPwVYtcu%2feSO1fkVHs%2b8o2XSkrxrYBIHtQxbJq4y9hBVRNV83h01KvxTaTnHq4l7O221CcxO6p5PM5eZwnencfpv4BPLE5oqQADRD77bsoXn9i5P%2bYwgZwGCSqGSIb3DQEHATAdBglghkgBZQMEAQIEEBzl5EvHK0rhP4981XSpvuOAcI7yOt9ZsRmxxLqvX25y3q7akXydsVPqOy7qOhGu6SUfEgiITyOymNmas9PlviV2iOHcCl3bcMQ6NaaqmPljsUULZFL1zrKdbhGbSe58zMETphwB9iRDrF%2bUtP36vpXNAUIe9ZOjA0bMKhOxuXD7r7s%3d; SAP_SESSIONID_SYV_100=1rw99umHHeJNPyo2HXhNPmPVnHdlAhHrq9n6Fj5FSBA%3d");
//			httpRequest.addHeader("Cookie","SAP_SESSIONID_SYV_100=cEvhPH-zLkc0qIbGBEVb7AAs9KBlfBHrlZj6Fj5FSBA%3d;");
			String authHeader = this.getAuthHeader(authType);
			dataIn = this.insertCustomHeaders(httpRequest, new URL(url), httpMethod, dataIn);	
			if (authHeader != null)
				httpRequest.addHeader("Authorization", authHeader);
			if ("GET".contentEquals(httpMethod))
				httpRequest.addHeader("x-csrf-token", "Fetch");
			if (!"GET".contentEquals(httpMethod)) {
				//Set the headers from DP for DELETE, POST, PUT, PATCH Requests
				if (readHeadersFromDPs) //else they were set from a prior GET
					setSecurityHeadersFromDocumentProperties(input.getDynamicProperties());
				setSecurityRequestHeaders(httpRequest, "POST".contentEquals(httpMethod));

				if (!"DELETE".contentEquals(httpMethod)) {
					InputStreamEntity reqEntity = new InputStreamEntity(dataIn);
					reqEntity.setContentType(getRequestContentType());

					switch (httpMethod) {
						case "POST":
							((HttpPost) httpRequest).setEntity(reqEntity);
							break;
						case "PATCH":
							((HttpPatch) httpRequest).setEntity(reqEntity);
							break;
						case "PUT":
							((HttpPut) httpRequest).setEntity(reqEntity);
							break;
					}
				}
			}
		
			response = httpClient.execute(httpRequest);
			if ("GET".contentEquals(httpMethod))
				this.getSecurityResponseHeaders(response);

		} finally {
			IOUtil.closeQuietly(dataIn);
//			if (httpClient!=null)
//				httpClient.close();
		}
		return response;
	}
    
    private String getAuthHeader(AuthType authenticationType) throws IOException, JSONException, GeneralSecurityException
    {
    	String authHeader = null;
       	if (authenticationType != null)
    	{
           	if (AuthType.BASIC == authenticationType)
        	{
            	String userpass = getUsername() + ":" + getPassword();
            	authHeader = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
        	} else if (AuthType.OAUTH==authenticationType) {
        		OAuth2Context oAuth2Context = _connProps.getOAuth2Context(ConnectionProperties.OAUTHOPTIONS.name());
        		OAuth2Token oAuth2Token = oAuth2Context.getOAuth2Token(false);
        		String accessToken = oAuth2Token.getAccessToken();
        		authHeader = "Bearer " + accessToken;
           	} else if (AuthType.CUSTOM==authenticationType)
           	{
           		return getCustomAuthHeader();
           	}
    	}
       	return authHeader;
	}
    
    protected HttpURLConnection openConnection(URL url, AuthType authenticationType) throws IOException, JSONException, GeneralSecurityException
    {
       HttpURLConnection connection = (HttpURLConnection) url.openConnection();
       String authHeader = this.getAuthHeader(authenticationType);
       if (authHeader!=null)
    	   connection.setRequestProperty ("Authorization", authHeader);	
       connection.setRequestProperty("Accept", "application/json");
       return connection;
    }
    
	/**
	 * Override to implement a Test Connection button on the Connections page. Default behavior is to open an authenticated connection to the base host URL
	*/
    public void testConnection() throws Exception
    {
    	CloseableHttpClient httpClient = HttpClients.createDefault();
    	CloseableHttpResponse httpResponse = null;
    	try {
        	logger.info("Test Connection: " +getTestConnectionUrl()+ " " + this.getAuthenticationType().name());
            HttpGet httpRequest = new HttpGet(this.getTestConnectionUrl());
            String authHeader = this.getAuthHeader(this.getAuthenticationType());
            if (authHeader!=null)
            	httpRequest.addHeader ("Authorization", authHeader);	
        	httpResponse = httpClient.execute(httpRequest);
        	if (200 != httpResponse.getStatusLine().getStatusCode())
        		throw new Exception(String.format("Problem connecting to endpoint: %s %s %s", this.getTestConnectionUrl(), httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase()));   			
    	} finally {
    		IOUtil.closeQuietly(httpResponse);
    	}
    }
    
	/**
	 * @return the maximum number of documents set by the user in the query operation page
	*/
    public long getMaxDocuments(PropertyMap opProps)
    {
    	return opProps.getLongProperty(ConnectionProperties.MAXDOCUMENTS.name(), -1L);
    }
    
	/**
	 * @return Returns the maximum depth set by the user in the import operations page for resolving recursive $ref during Import/Browse
	*/
    public long getMaxBrowseDepth(PropertyMap opProps)
    {
    	long defaultDepth=0L;
    	long maxDepth=5L;
    	long depth = opProps.getLongProperty(ConnectionProperties.MAXBROWSEDEPTH.name(), defaultDepth);
    	if (depth>maxDepth)
    		depth=maxDepth;
    	return depth;
    }
    
	/**
	 * @return the page size set by the user in the query operation page
	*/
    public long getPageSize(PropertyMap opProps)
    {
        return opProps.getLongProperty(ConnectionProperties.PAGESIZE.name(), 100L);
    }
        
	/**
	 * @return the username set by the user in the Connections Page
	*/
    protected String getUsername()
    {
    	return _username;
    }
    
	/**
	 * @return the password set by the user in the Connections Page
	*/
    protected String getPassword()
    {
    	return _password;
    }
    
	/**
	 * For query operations, provide a JSON path to the schema for an individual item. For example /properties/items/* 
	 * @return the JSON path to the pagination item type
	*/
    public String getQueryPaginationItemPath()
    {
    	return "";
    }
    
	/**
	 * For example, there may be APIs for authentication in the swagger file that should be hidden from the user 
	 * @return the list of ids to exclude in the format <path>___<http method>
	*/
    public List<String> getExcludedObjectTypeIDs()
    {
    	return null;
    }
    
	/**
	 * Returns the AuthType enumeration to set the default authentication type. Defaults to AuthType.NONE
	 * @return AuthType value NONE | BASIC | OAUTH20 | CUSTOM
	*/
    protected AuthType getAuthenticationType()
    {
    	return this._authenticationType;
    }
    
	/**
	 * Returns the service url/host set by the user in the Connections page.
	 * @return the url for the service
	*/
    protected String getBaseUrl()
    {
    	String baseURL = _connProps.getProperty(ConnectionProperties.URLALT.name(), "").trim();
    	if (baseURL.length()==0)
    	{
        	baseURL = _connProps.getProperty(ConnectionProperties.URL.name(), "").trim();    		
    		String servicePath = _connProps.getProperty(ConnectionProperties.SERVICEPATH.name(), "").trim();
    		baseURL+=servicePath;
    	}
		if (baseURL.length()==0)
			throw new ConnectorException("A Service URL is required");
		if (!baseURL.endsWith("/"))
			baseURL+="/";
    	return baseURL;
    }
    
	/**
	 * Used by operations to set the logger to the Process Log logger
	 * @param the Logger
	*/
    public void setLogger(Logger logger)
    {
    	this.logger = logger;
    }
    
	/**
	 * Returns the Java logger so methods can can log to the container logs during Import, or the Process Log during process execution.
	 * @return the Logger
	*/
    protected Logger getLogger()
    {
    	return this.logger;
    }

	/**
	 * Returns the Connection Properties set by the user in the connectors Connection page.
	 * @return the connection PropertyMap
	*/
    protected PropertyMap getConnectionProperties()
    {
    	return this._connProps;
    }
    
    //TODO this should come from swagger but fast hack to support stripe request form-encoded
    protected String getRequestContentType()
    {
    	return "application/json";
    }
    
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl();
    }
    
	/**
	 * Allows the addition of custom headers to the connection. For example for Visa Cybersource, this is used to implement HTTP Signature Authentication
	 * @param httpRequest the connection for which you can add headers via the addRequestProperty method
	 * @param url the url for the request
	 * @param method the HTTP method for the request
	 * @param data the InputStream for the request body 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
	protected InputStream insertCustomHeaders(HttpRequestBase httpRequest, URL url, String method, InputStream data)
	{
		return data;
	}
	/**
	 * Provide a custom Authorization header for AuthType.CUSTOM implementations.
	 * Useful for proprietary API driven authentication. 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
    protected String getCustomAuthHeader() throws JSONException, IOException, GeneralSecurityException
    {
    	return null;
    }

	public String getAcceptHeader() {
		return acceptHeader;
	}

	public void setAcceptHeader(String acceptHeader) {
		this.acceptHeader = acceptHeader;
	}

	public String getxCSRFToken() {
		return xCSRFToken;
	}

	public void setxCSRFToken(String xCSRFToken) {
		this.xCSRFToken = xCSRFToken;
	}

	public String getSessionCookies() {
		return sessionCookies;
	}

	public void setSessionCookies(String sessionCookies) {
		this.sessionCookies = sessionCookies;
	}

	public String geteTag() {
		return eTag;
	}

	public void seteTag(String eTag) {
		this.eTag = eTag;
	}    
	
	//TODO the etag value might be in the response...meaning we have to get it when converting the payload from OData to JSON
	/**
	 * Called after a GET request to grab security headers
	 * @param httpResponse
	 */
	private void getSecurityResponseHeaders(CloseableHttpResponse httpResponse)
	{
    	Header[] xCSRFTokens = httpResponse.getHeaders("x-csrf-token");
    	if (xCSRFTokens.length>0)
    		this.xCSRFToken = xCSRFTokens[0].getValue();
		
    	Header[] etags = httpResponse.getHeaders("etag");
    	if (etags.length>0)
    		this.eTag = etags[0].getValue();
    	else
    		this.eTag=null; //It is a good decsion to clear any existing etag to force it to be scanned from the response payload
    	Header[] cookies = httpResponse.getHeaders("set-cookie");
		String cookieString="";
    	for (Header cookie : cookies)
    	{
    		cookieString+=cookie.getValue()+";";
    	}
    	this.sessionCookies = cookieString;
	}
	
	/**
	 * Write etag, cookie, xcsrf token to outbound document properties
	 * @param context
	 * @return
	 */
	protected PayloadMetadata setSecurityHeaderDocumentProperties(OperationContext context)
	{
    	PayloadMetadata payloadMetadata = context.createMetadata();    
    	//Get the headers from the GET, QUERY response
    	if (StringUtil.isNotBlank(xCSRFToken))
    		payloadMetadata.setTrackedProperty("X_CSRF_TOKEN", xCSRFToken);

    	if (StringUtil.isNotBlank(eTag))
    		payloadMetadata.setTrackedProperty("ETAG", eTag);
    	
    	if (StringUtil.isNotBlank(sessionCookies)  && sessionCookies.contains("SAP_SESSIONID"))
    		payloadMetadata.setTrackedProperty("SAP_SESSIONID_COOKIE", sessionCookies);
    	return payloadMetadata;
	}
		
	/**
	 * Read etag, cookie, xcsrf from inboud document properties
	 * @param dynamicProperties
	 */
	void setSecurityHeadersFromDocumentProperties(Map<String, String> dynamicProperties)
	{
		xCSRFToken = dynamicProperties.get("X_CSRF_TOKEN");
		eTag = dynamicProperties.get("ETAG");        
        sessionCookies = dynamicProperties.get("SAP_SESSIONID_COOKIE");
	}
	
	void setSecurityRequestHeaders(HttpRequestBase httpRequest, boolean isPost)
	{
        if(!StringUtil.isEmpty(xCSRFToken))
        	httpRequest.addHeader("x-csrf-token", xCSRFToken); //Set the x-csrf-token header
        else
            throw new ConnectorException("The 'X-CSRF Token' Document Property must be set. This value is returned in the Document Property returned by GET operations.");

        if (!isPost) //etag not required for post but what about DELETE?
        {
    	    if(!StringUtil.isEmpty(eTag)) 
    	    	httpRequest.addHeader("If-Match", eTag); //Set the header If-Match etag
//    	    else
//	    	throw new ConnectorException("The 'ETag/Document Version Number' Document Property must be set. This value is returned in the Document Property returned by GET operations.");
        }
        
	    if (!StringUtil.isEmpty(sessionCookies) && sessionCookies.contains("SAP_SESSIONID"))
	    {
	    	httpRequest.addHeader("Cookie", sessionCookies); 
	    }
	}
}
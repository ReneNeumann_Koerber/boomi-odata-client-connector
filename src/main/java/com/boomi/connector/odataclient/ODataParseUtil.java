package com.boomi.connector.odataclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import com.boomi.connector.api.ConnectorException;
import com.boomi.util.IOUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class ODataParseUtil {
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSSZZ");
	/** 
	 * Parse an inbound document payload
	 * Convert JSON dates, times etc to OData formats
	 * Extract keys and return a formated URL predicate
	 * @param is Input stream of JSON
	 * @param os Output stream of parsed JSON. For GET and DELETE operations, there will be no output stream and only predicate keys are returned
	 * @param inputCookie Contains the property metadata for properties, keys and navigation properties
	 * @return the predicate key for building the URL
	 * @throws JsonParseException
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static String parseBoomiToOData(InputStream is, OutputStream os, OperationCookie inputCookie) throws JsonParseException, IOException, ParseException {
		StringBuilder keys = new StringBuilder();
		String firstKeyValue = null;
		long numberKeys=0;
		StringBuilder pathPointer = new StringBuilder();
		JsonFactory JSON_FACTORY = new JsonFactory();
		JsonGenerator generator = null;
		JsonParser parser = null;
		String lastValue = "";
		// open the parser and generator as managed resources that are guaranteed to be closed
		try {
			parser = JSON_FACTORY.createParser(is);
			if (os!=null)
				generator = JSON_FACTORY.createGenerator(os);	 
			while (parser.nextToken() != null) {
				JsonToken element = parser.getCurrentToken();
				if (element == JsonToken.END_ARRAY) {
					popElement(pathPointer);
					if (os!=null)
						generator.writeEndArray();
				}
				if (element == JsonToken.START_OBJECT && os!=null)
					generator.writeStartObject();

				if (element == JsonToken.FIELD_NAME) {
					String currentName = parser.getCurrentName();
					pushElement(pathPointer, currentName);
					// grab the value token
					element = parser.nextToken();
					// write the current field name
					if (os!=null)
						generator.writeFieldName(currentName);

					if (element == JsonToken.START_OBJECT && os!=null)
						generator.writeStartObject();
					else if (element == JsonToken.START_ARRAY && os!=null)
						generator.writeStartArray();
					else if (element.name().startsWith("VALUE_")) {
						lastValue=pathPointer.toString();
						String edmType = inputCookie.getEdmType(pathPointer.toString());
						if (edmType!=null)
						{
							if (inputCookie.isKey(pathPointer.toString()) && pathPointer.lastIndexOf("/")==0) //Top level key,
							{
								numberKeys++;
								String odataValue=ODataEdmType.boomiValuetoODataPredicate(parser.getValueAsString(), edmType);
								if (keys.length()>0)
									keys.append(",");
								else 
									firstKeyValue = odataValue;
								keys.append(pathPointer.toString().substring(1)+"="+odataValue);							
							}
							if (os!=null)
							{
								String value = parser.getValueAsString();
								if (value==null)
									generator.writeNull();
								else {
									switch (edmType)
									{
										case "Single":
										case "Double":
										case "Decimal":
											generator.writeString(String.format("%f", parser.getValueAsDouble()));									
											break;
										case "DateTimeOffset":
										case "DateTime":
											//"LastChangeDate": "/Date(1588377600000)/",
											dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
											value = String.format("/Date(%d)/", dateFormat.parse(value).getTime());
											generator.writeString(value);
											break;
										case "Time":
							                //"LastChangeTime": "PT14H27M30S",
											timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
											Date dt = timeFormat.parse(value);
											value = String.format("PT%02dH%02dM%02dS", dt.getHours(), dt.getMinutes(), dt.getSeconds());
											generator.writeString(value);
											break;
										default:
											writeJsonTokenToGenerator(element, generator, parser);
											break;
									}
								}
							} 
						} else {
							writeJsonTokenToGenerator(element, generator, parser);
						}
						popElement(pathPointer);
					}
				}
				
				if (element == JsonToken.END_OBJECT && !parser.getParsingContext().inRoot())
				{
					if (os!=null)
						generator.writeEndObject();
					popElement(pathPointer);
				}
			}
			// flush the generator
			if (os!=null)
			{
				generator.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new ConnectorException(e.toString() + " " + pathPointer.toString() + ":" + lastValue);
		} finally {
			IOUtil.closeQuietly(parser);
			IOUtil.closeQuietly(is);
			IOUtil.closeQuietly(generator);
		}
		String predicate;
		if (numberKeys==1)
			predicate=firstKeyValue;
		else
			predicate=keys.toString();
		if (numberKeys==0)
			return "";
		return "(" + predicate + ")";
	}

	/**
	 * Parse the inbound document, remove any root "d" object and convert OData date, time, decimal types to boomi/json
	 * TODO parser will leave blank arrays of primitives like [1,2,3]
	 * @param is Input stream of JSON
	 * @param os Output stream of parsed JSON. OS can be null if all we are doing is parsing for an etag
	 * @param _operationCookie Contains the property metadata for properties, keys and navigation properties
	 * @return any ETag value found in the response payload
	 * @throws JsonParseException
	 * @throws IOException
	 */
	public static String parseODataToBoomi(InputStream is, OutputStream os, OperationCookie operationCookie) {
		StringBuilder pathPointer = new StringBuilder();
		JsonFactory JSON_FACTORY = new JsonFactory();
		// open the parser and generator as managed resources that are guaranteed to be
		// closed
		JsonParser parser = null;
		JsonGenerator generator = null;
		String lastValue="";
		String eTag = null;
		try {
			parser = JSON_FACTORY.createParser(is);
			if (os!=null)
				generator = JSON_FACTORY.createGenerator(os);
			while (parser.nextToken() != null) {
				JsonToken element = parser.getCurrentToken();
				if (element == JsonToken.END_ARRAY) {
					popElement(pathPointer);
					if (os!=null)
						generator.writeEndArray();
				}
				if (element == JsonToken.START_OBJECT && os!=null)
					generator.writeStartObject();

				if (element == JsonToken.FIELD_NAME) {
					String currentName = parser.getCurrentName();
					pushElement(pathPointer, currentName);
					// grab the value token
					element = parser.nextToken();
//					if ("/BOMHeaderQuantityInBaseUnit".contentEquals(pathPointer.toString()))
//						System.out.println();
					//Ensure not in the root but rather in fields below /d
					if (!"/d".contentEquals(pathPointer)) {
						// write the current field name
						if (os!=null)
							generator.writeFieldName(currentName);

						if (element == JsonToken.START_OBJECT && os!=null)
							generator.writeStartObject();
						else if (element == JsonToken.START_ARRAY && os!=null)
							generator.writeStartArray();
						else if (element.name().startsWith("VALUE_")) {
							String cookieKey=pathPointer.toString();
							lastValue = parser.getValueAsString();
							if ("ETag".contentEquals(currentName))
								eTag = lastValue;
							if (cookieKey.startsWith("/d/"))
								cookieKey=cookieKey.substring(2);
							String edmType = operationCookie.getEdmType(cookieKey);
							if (edmType!=null && os!=null)
							{
								String value = lastValue;
								if (value==null)
									generator.writeNull();
								else {
									switch (edmType)
									{
										case "Single":
										case "Double":
										case "Decimal":
											double number = Double.parseDouble(value);
											generator.writeNumber(number);
											break;
										case "DateTimeOffset":
											//"LastChangeDate": "/Date(1588377600000+0000)/",
											boolean isMinus=false;
											int offsetPos = value.indexOf("+");
											if (offsetPos==-1)
											{
												offsetPos = value.indexOf("-");
												if (offsetPos>0)
													isMinus = true;
												else
													throw new ConnectorException("Invalid DateTimeOffset value: " + value);
											}
											Long base=Long.parseLong(value.substring(value.indexOf("(")+1, offsetPos));
											Long offset = Long.parseLong(value.substring(offsetPos+1, value.lastIndexOf(")")));
											if (isMinus)
												base-=offset;
											else
												base+=offset;
												
											value=dateFormat.format(new Date(base));
											generator.writeString(value);
											break;
										case "DateTime":
											//"LastChangeDate": "/Date(1588377600000)/",
											value=value.substring(value.indexOf("(")+1, value.lastIndexOf(")"));
											dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
											value=dateFormat.format(new Date(Long.parseLong(value)));
											generator.writeString(value);
											break;
										case "Time":
							                //"LastChangeTime": "PT14H27M30S",
											int hours = Integer.parseInt(value.substring(2,4));
											int minutes = Integer.parseInt(value.substring(5,7));
											int seconds = Integer.parseInt(value.substring(8,10));
											Date dt = new Date();
											dt.setHours(hours);
											dt.setMinutes(minutes);
											dt.setSeconds(seconds);
											timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
											generator.writeString(timeFormat.format(dt));
											break;
										default:
											writeJsonTokenToGenerator(element, generator, parser);
											break;
										}
								}
							} else {
								writeJsonTokenToGenerator(element, generator, parser);
							}
							popElement(pathPointer);
						}
					}
				}
				
				if (element == JsonToken.END_OBJECT && !parser.getParsingContext().inRoot()) // We whacked /d so ignore the last pop
				{
					if (os!=null)
						generator.writeEndObject();
					popElement(pathPointer);
				}
			}
			// flush the generator
			if (os!=null)
				generator.flush();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ConnectorException(e.toString() + " " + pathPointer.toString() + ":" + lastValue);
		} finally {
			IOUtil.closeQuietly(is);
			IOUtil.closeQuietly(generator);
			IOUtil.closeQuietly(parser);
		}
		return eTag; //TODO this only returns a single Etag, assumes that the response only includes a single entitytype
	}

	public static String parseBoomiToFunctionImportURL(InputStream is, OperationCookie inputCookie) {
		StringBuilder queryParameters = new StringBuilder();
		JsonFactory JSON_FACTORY = new JsonFactory();
		JsonParser parser = null;
		String currentName = "";
		// open the parser and generator as managed resources that are guaranteed to be closed
		try {
			parser = JSON_FACTORY.createParser(is);
			while (parser.nextToken() != null) {
				JsonToken element = parser.getCurrentToken();

				if (element == JsonToken.FIELD_NAME) {
					currentName = parser.getCurrentName();
					// grab the value token
					element = parser.nextToken();
					// write the current field name

					if (element.name().startsWith("VALUE_")) {
						String edmType = inputCookie.getEdmType("/"+currentName);
						if (edmType!=null)
						{
							String odataValue=ODataEdmType.boomiValuetoODataPredicate(parser.getValueAsString(), edmType);
							if (queryParameters.length()>0)
								queryParameters.append("&");
							queryParameters.append(currentName+"="+odataValue);							
						}
					}
				}
			}
			// flush the generator
		} catch (IOException e) {
			e.printStackTrace();
			throw new ConnectorException(e.toString() + " " + currentName);
		} finally {
			IOUtil.closeQuietly(parser);
			IOUtil.closeQuietly(is);
		}
		return queryParameters.toString();
	}

	private static void pushElement(StringBuilder pathPointer, String name) {
		pathPointer.append("/");
		pathPointer.append(name);
//		System.out.println("PUSHED " + pathPointer);
	}

	private static void popElement(StringBuilder pathPointer) {
		int lastPos = pathPointer.lastIndexOf("/");
		if (lastPos > -1)
			pathPointer.setLength(lastPos);
//		System.out.println("POPPED " + pathPointer);
	}

	private static void writeJsonTokenToGenerator(JsonToken element, JsonGenerator generator, JsonParser parser) throws IOException
	{
		if (generator!=null)
		{
			if (parser.getValueAsString()==null)
				generator.writeNull();
			else
			{
				switch (element)
				{
				case VALUE_STRING:
					generator.writeString(parser.getValueAsString());
					break;
				case VALUE_TRUE:
					generator.writeBoolean(true);
					break;
				case VALUE_FALSE:
					generator.writeBoolean(false);
					break;
				case VALUE_NUMBER_INT:
					generator.writeNumber(parser.getValueAsLong());
					break;
				case VALUE_NUMBER_FLOAT:
					generator.writeNumber(parser.getValueAsDouble());
					break;
				case VALUE_NULL:
					generator.writeNull();
					break;
				default:
					throw new ConnectorException("Unhandled JSON Value Type: " + element.name());								
				}
			}
		}
	}
}

package com.boomi.connector.odataclient;

import java.io.IOException;
import java.util.List;

import org.apache.olingo.odata2.api.edm.EdmAnnotationAttribute;
import org.apache.olingo.odata2.api.edm.EdmAnnotations;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmFacets;
import org.apache.olingo.odata2.api.edm.EdmProperty;
import org.json.JSONException;
import org.json.JSONObject;

import com.boomi.connector.api.ConnectorException;

public class ODataEdmType {

	static String boomiValuetoODataPredicate(String value, String edmTypeName) throws IOException {
		String predicate;
		switch (edmTypeName)
		{
		case "Byte":
		case "Int16":
		case "Int32":
		case "Int64":
			predicate=value;
			break;
		case "Single":
		case "Double":
		case "Decimal":
			predicate=value; //TODO
			break;
		case "Boolean":
			predicate=value;
			break;
		case "DateTimeOffset":
		case "DateTime":
			predicate=edmTypeName.toLowerCase()+"'"+value+"'";
			break;
		case "Time":
			predicate=value; //TODO
			break;
		case "Binary": //Binary means base64 encoded
		case "String":
			predicate="'"+value.replace("'", "''")+"'"; //escape single quotes
			break;
		case "Guid":
			predicate="guid'"+value+"'";
			break;
		default:
			throw new ConnectorException("Unsupported EDM Type: " + edmTypeName);
		}
		
		return predicate;
	}

	static void buildSchemaType(String edmTypeName, JSONObject property)
	{
		String typeName;
		String format=null;
		switch (edmTypeName)
		{
		case "Byte":
		case "Int16":
		case "Int32":
		case "Int64":
			typeName="integer";
			break;
		case "Single":
		case "Double":
		case "Decimal":
			typeName="number";
			break;
		case "Boolean":
			typeName="boolean";
			break;
		case "DateTimeOffset":
		case "DateTime":
			typeName="string";
			format="date-time";
			break;
		case "Time":
			typeName="string";
			format="time";
			break;
		case "Binary": //Binary means base64 encoded
		case "String":
			typeName="string";
			break;
		case "Guid":
			property.put("maxLength", 36);
//			property.put("minLength", 36); //TODO minLength will force it to be required
			typeName="string";
			break;
		default:
			throw new ConnectorException("Unsupported EDM Type: " + edmTypeName);
		}
		property.put("type", typeName);
		if (format!=null)
			property.put("format", format);
	}
	
	static void buildSchemaProperty(EdmProperty type, JSONObject property) throws EdmException
	{
		EdmAnnotations annotations = type.getAnnotations();
		List<EdmAnnotationAttribute> attributes = annotations.getAnnotationAttributes();
		if (attributes!=null)
		{
			for (EdmAnnotationAttribute attribute : attributes)
			{
				switch (attribute.getName())
				{
				case "quickinfo":
					property.put("description", attribute.getText());
					break;
				}
			}
		}
//		annotations.
		EdmFacets facets = type.getFacets();
		if (facets!=null)
		{
			//TODO Required, pattern others???
			if (facets.getMaxLength()!=null)
				property.put("maxLength", facets.getMaxLength());
//			if (facets.isNullable())
		}
		String edmTypeName = type.getType().getName();
		buildSchemaType(edmTypeName, property);
	}
}

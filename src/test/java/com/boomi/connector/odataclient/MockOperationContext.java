package com.boomi.connector.odataclient;

import java.util.List;
import java.util.Map;

import com.boomi.connector.api.AtomConfig;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.testutil.SimpleOperationContext;

public class MockOperationContext extends SimpleOperationContext {

	private List<String> selectedFields;
	private String customOperationType;
	public MockOperationContext(AtomConfig config, Connector connector, OperationType opType,
			Map<String, Object> connProps, Map<String, Object> opProps, String objectTypeId,
			Map<ObjectDefinitionRole, String> cookies, List<String> selectedFields) {
		super(config, connector, opType, connProps, opProps, objectTypeId, cookies, selectedFields);
	}

//	@Override
//	public List<String> getSelectedFields() {
//		return selectedFields;
//	}

	@Override
	public String getCustomOperationType() {
		return customOperationType;
	}

	public void setCustomOperationType(String customOperationType) {
		this.customOperationType = customOperationType;
	}

}
